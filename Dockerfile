FROM nginx:stable-alpine

WORKDIR /webrtc

ADD . /webrtc

EXPOSE 5000 80

RUN apk update && apk add python3 python3-dev python-dev py-pip musl-dev \
    linux-headers gcc git && pip install -r requirements.txt && pip install \
    git+https://github.com/Supervisor/supervisor

RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

ENTRYPOINT /webrtc/docker-entrypoint.sh

CMD /bin/sh
