### Backend WebRTC

#### Installation

#### In product

```bash
$ pip install -r requirements.txt

$ pip install git+https://github.com/Supervisor/supervisor

$ sudo cp webrtc /etc/nginx/sites-availabel

$ sudo ln -s /etc/nginx/sites-availabel /etc/nginx/sites-enabled

$ sudo service nginx restart

$ supervisord
```

#### In dev

```bash
$ virtualenv env -p python3

$ env/bin/pip install -r requirements.txt

$ env/bin/python run_dev.py runserver

```

### Acknowledge

* Tìm hiểu Restful API [Document](https://medium.com/@MLSDevCom/a-beginners-tutorial-for-understanding-restful-api-e7426283efba) [Demo](https://github.com/nguyenkhacthanh/restful_api)

* Tìm hiểu authenticate với JWT [Document](https://medium.com/vandium-software/5-easy-steps-to-understanding-json-web-tokens-jwt-1164c0adfcec)

* Python3.6.x

* virtualenv tạo môi trường ảo(cô lập) trong quá trình dev

* Hiểu variable scope, module, package trong python3

* Tìm hiểu thiết kế framework Flask(application context, request context, global instance) [Document](http://flask.pocoo.org/docs/1.0/)

* Tìm hiểu các module mở rộng của Flask (ORM lib Flask-SQLAlchemy, Restful API với Flask-Restful)

* Mở rộng Docker, Nginx, WSGI
