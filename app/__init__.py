"""
Business logic service for WebRCT App
"""
import logging
from flask import Flask, render_template
from flask_script import Manager, Shell
from flask_moment import Moment
from flask_migrate import Migrate, MigrateCommand
from flask_restful import Api

from . import config
from . import models
from . import utils
from . import blueprints
from . import resources
from . import socket


def create_app(config_name):
    """
    Create instance app by factory pattern

    Parameters:
    -----------
    config_name: str, name of config for flask object

    Returns:
    --------
    manager: instance of Manager
    """
    app = Flask(__name__)
    app.config.from_object(config.get_config(config_name))
    manager = Manager(app)
    socket.io.init_app(app)
    moment = Moment(app)
    models.db.init_app(app)
    migrate = Migrate(app, models.db)
    api = Api(blueprints.api_bp)

    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    def _make_context():
        return dict(app=app, db=models.db, models=models)

    manager.add_command('db', MigrateCommand)
    manager.add_command('shell', Shell(
        make_context=_make_context,
        use_ipython=True))

    api.add_resource(resources.User, '/user')
    api.add_resource(resources.Group, '/group')
    api.add_resource(resources.Authentication, '/auth')
    api.add_resource(resources.HelloWorld, '/ping')

    app.register_blueprint(blueprints.api_bp, url_prefix='/api')
    @app.route('/socketio')
    def socketio():
        return render_template('socketio.html')

    if config_name == 'ProductConfig':
        return app
    else:
        return manager
