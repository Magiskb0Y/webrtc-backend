"""Modular app
"""
from flask import Blueprint


api_bp = Blueprint('api', __name__)
socketio_bp = Blueprint('socketio', __name__)
