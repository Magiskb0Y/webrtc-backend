"""
Configure module for backend
"""
import os
from app.utils import generate_key


_BASEDIR = os.getcwd()


class BaseConfig(object):
    DEBUG = True
    TESTING = False
    SECRET_KEY = generate_key(length=32)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.join(_BASEDIR, 'dev.sqlite3')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    HOST = '127.0.0.1'
    PORT = 5000


class DevelopmentConfig(BaseConfig):
    pass


class ProductConfig(BaseConfig):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://username:password@localhost:3306/webrtc_backend'


def get_config(config_name):
    return {
            'DevelopmentConfig': DevelopmentConfig,
            'ProductConfig': ProductConfig
            }.get(config_name, DevelopmentConfig)
