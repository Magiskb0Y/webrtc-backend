"""
Kurento communication helper
"""

import asyncio
import websockets
from functools import wraps


class KurentoClient(object):
    def __init__(self, host='ws://localhost', port=8888, path='kurento'):
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self._create_connect(host, port, path))
        self.responses = dict()

    async def _create_connect(self, host, port, path):
        """Create async connect to websocket server
        Parameters:
        -----------
        host: str, hostname of server
        port: int, number of port
        path: str, endpoint

        Returns:
        """
        self.websocket_conn = await websockets.connect('{}:{}/{}'.format(host, port, path))

    async def recv(self):
        """Run other thread for listen return response from kurento server
        """
        async def _wait_response():
            """Async function listen"""
            response = await self.websocket_conn.recv()
            return response
        self.loop.run_forever(_wait_response())

    async def _send(self, func):
        """Decorator function send request to kurento server by websocket
        Parameters:
        -----------
        f: objet function, json generate function

        Returns:
        --------
        function with decorate

        """
        @wraps(func)
        async def in_func(*args, **kwargs):
            """Async function
            """
            payload = func(*args, **kwargs)
            await self.websocket_conn.send(payload)
            response = await self.websocket_conn.recv()
            return response
        return in_func

    @_send
    def ping(self, idx=1, interval=240000):
        """Function check server kurento
        Parameters:
        -----------
        interval: int

        Returns:
        payload: dict, payload of request to kurento server
        """
        payload = {
            'id': idx,
            'method': 'ping',
            'params': {
                'interval': interval
            },
            'jsonrpc': '2.0'
        }
        return payload

    @_send
    def create_pipeline(self, idx=2):
        """Function create pipeline media to server kurento
        Parameters:
        -----------
        id: int

        Returns:
        payload: dict, payload of request to kurento server
        """
        payload = {
            'id': idx,
            'method': 'create',
            'params': {
                'type': 'MediaPipeline',
                'constructorParams': {},
                'properties': {}
            },
            'jsonrpc': '2.0'
        }
        return payload

    @_send
    def create_endpoint(self, value, sessionId, idx=3):
        """Function create endpoint server kurento
        Parameters:
        -----------
        id: int

        Returns:
        payload: dict, payload of request to kurento server
        """
        payload = {
            'id': idx,
            'method': 'create',
            'params': {
                'type': 'WebRtcEndpoint',
                'constructorParams': {
                    'mediaPipeline': value
                },
                'sessionId': sessionId
            },
            'jsonrpc': '2.0'
        }
        return payload

    @_send
    def release_resource(self, value, sessionId, idx=4):
        """Function destroy object in server kurento
        Parameters:
        -----------
        value: str
        sessionId: str
        id: int

        Returns:
        payload: dict, payload of request to kurento server
        """
        payload = {
            'id': idx,
            'method': 'release',
            'params': {
                'object': value,
                'sessionId': sessionId
            },
            'jsonrpc': '2.0'
        }
        return payload
