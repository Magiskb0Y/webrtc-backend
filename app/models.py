"""
Define models in database
"""
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash


db = SQLAlchemy()


class User(db.Model):
    """Define user model
    """
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, index=True)
    first_name = db.Column(db.String(20), nullable=False)
    last_name = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(50), nullable=False, unique=True)
    password_hash = db.Column(db.String(128), nullable=False)
    address = db.Column(db.String(200), nullable=True)
    on_register = db.Column(db.DateTime, nullable=False, default=datetime.now)
    user_group = db.relationship('UserGroup', backref='users')

    @property
    def password(self):
        """Private attribute
        """
        raise AttributeError('This is readable only attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Perform check password
        Parameters:
        -----------
        password: str, original password

        Returns:
        --------
        check: bool, True if password_hash equal hash string of password,
               else False
        """
        return check_password_hash(self.password_hash, password)

    def __init__(self, first_name, last_name, email, password, address):
        """Initial user
        Parameters:
        -----------
        first_name: str, length < 20
        last_name: str, length < 20
        email: str, length < 50
        password: str
        address: str, length < 200

        Returns:
        --------
        """
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
        self.address = address
        self.on_register = datetime.now()

    def save_to_db(self):
        """Flush object to database
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def check_exist_by_email(cls, email):
        """Check instance in table
        """
        user = cls.query.filter_by(email=email).first()
        return user is not None

    @classmethod
    def to_json(cls, x):
        """Serialize object to json
        Parameters:
        -----------
        x: object of user

        Returns:
        --------
        object: dict
        """
        return {
                'id': x.id,
                'email': x.email,
                'name': {
                    'first_name': x.first_name,
                    'last_name': x.last_name
                },
                'address': x.address,
                'on_register': x.on_register.strftime('%D-%B-%Y')
        }

    @classmethod
    def get_all(cls):
        """Get all user
        """
        return {'users': list(map(lambda x: cls.to_json(x), User.query.all()))}

    @classmethod
    def get_user_by_email(cls, email):
        """Get user by email
        Parameters:
        -----------
        email: str

        Returns:
        user: dict
        """
        user = cls.query.filter_by(email=email).first()
        if user is None:
            return None
        else:
            return cls.to_json(user)

    @classmethod
    def get_user_by_id(cls, id):
        """Get user by id
        Parameters:
        -----------
        id: int

        Returns:
        user: dict
        """
        user = cls.query.filter_by(id=id).first()
        if user is None:
            return None
        else:
            return cls.to_json(user)

    @classmethod
    def delete_all(cls):
        num_rows_deleted = db.session.query(cls).delete()
        db.session.commit()
        return {'num_rows_deleted': num_rows_deleted}

    def __repr__(self):
        return '<User id: {} email: {}>'.format(self.id, self.email)


class Group(db.Model):
    """Define group model
    """
    __tablename__ = 'groups'

    id = db.Column(db.Integer, primary_key=True, index=True)
    session = db.Column(db.String(30), nullable=False, unique=True)
    describe = db.Column(db.String(200), nullable=True)
    status = db.Column(db.String(10), nullable=False)
    on_start = db.Column(db.DateTime, nullable=False, default=datetime.now)
    on_finish = db.Column(db.DateTime, nullable=True)
    user_group = db.relationship('UserGroup', backref='groups')

    def __init__(self, session, describe, status):
        """Initial group model
        Parameters:
        -----------
        session: str, session of Kurento Media Pipeline
        describe: str
        status: str, one of values [`public`, `private`]

        Returns:
        --------
        """
        self.session = session
        self.describe = describe
        self.status = status
        
    def save_to_db(self):
        """Flush object to database
        """
        db.session.add(self)
        db.session.commit()
    
    @classmethod
    def to_json(cls, x):
        """Serialize object of group to json
        Parameters:
        -----------
        x: object of group class

        Returns:
        --------
        object: dict
        """
        return {
                'id': x.id,
                'session': x.session,
                'status': x.status,
                'on_start': x.on_start.strftime('%D-%M-%Y %H:%M:%S'),
                'on_finish': x.on_finish.strftime('%D-%M-%Y %H:%M:%S')
        }

    @classmethod
    def get_group_by_id(cls, id):
        """Perform get group by id
        Parameters:
        -----------
        id: int

        Returns:
        --------
        dict object
        """
        group = cls.query.filter_by(id=id).first()
        if group is None:
            return None
        else:
            return {
                'id': group.id,
                'session': group.session,
                'status': group.status,
                'on_start': group.on_start.strftime('%D-%B-%Y %H:%M:%S')
            }

    @classmethod
    def get_all(cls, x):
        """Get all group
        """
        return {'groups': list(map(lambda x: cls.to_json(x), cls.query.all()))}

    @classmethod
    def delete_all(cls):
        num_rows_deleted = db.session.query(cls).delete()
        db.session.commit()
        return {'num_rows_deleted': num_rows_deleted}

    def __repr__(self):
        return '<Group id: {} session: {}>'.format(self.id, self.session)


class Role(db.Model):
    """Define role model
    """

    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True, index=True)
    title = db.Column(db.String(10), nullable=False, unique=True)
    summary = db.Column(db.String(200), nullable=True)
    user_group = db.relationship('UserGroup', backref='roles')

    def __init__(self, title, summary):
        """Initial role model
        Parameters:
        -----------
        title: str, length < 10
        summary: str, length < 200

        Returns:
        --------
        """
        self.title = title
        self.summary = summary

    def __repr__(self):
        return '<Role id: {} title: {}>'.format(self.id, self.title)


class UserGroup(db.Model):
    """Define many-to-many relationship between user and group when call
    """

    __tablename__ = 'user2group'

    id = db.Column(db.Integer, primary_key=True, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    group_id = db.Column(db.Integer, db.ForeignKey('groups.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    on_connect = db.Column(db.DateTime, nullable=False, default=datetime.now)
    on_leave = db.Column(db.DateTime, nullable=False)
    is_ban = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, user_id, group_id, role_id, on_connect, on_leave,
                 is_ban):
        """Inital model
        Parameters:
        -----------
        user_id: int
        group_id: int
        role_id: int
        on_connect: datetime, default now
        on_leave: datetime
        is_ban: bool, default: False
        """
        self.user_id = user_id
        self.group_id = group_id
        self.role_id = role_id
        self.on_connect = on_connect
        self.on_leave = on_leave
        self.is_ban = is_ban

    def __repr__(self):
        return '<UserGroup id: {} user: {} group: {}>'.format(self.id,
                                                              self.user_id,
                                                              self.group_id)

