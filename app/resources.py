"""
Define resouces for RestApi
"""
from functools import wraps
from datetime import datetime

import jwt
from flask import jsonify, request
from flask_restful import reqparse, Resource

from app import models


JWT_KEY = 'secret_key'
JWT_ALGORITHM = 'HS256'
JWT_TIME_EXPIRED = 60 * 60 * 24


def auth(f):
    @wraps(f)
    def func_produre(*args, **kwargs):
        token = request.headers.get('Authorization', None)
        if token is None:
            return jsonify(message='Token is missing', code=1), 400
        else:
            try:
                payload = jwt.decode(token, JWT_KEY, JWT_ALGORITHM)
                now_timestamp = datetime.timestamp(datetime.now())
                valid = now_timestamp < payload.pop('expired')
                if not valid:
                    raise ValueError('Token expired')
                user_existed = models.User.check_exist_by_email(payload['email'])
                if not user_existed:
                    raise ValueError('User not exist')
                kwargs.update(payload)
            except Exception:
                return jsonify(message='Token is invalid', code=1), 400
        return f(*args, **kwargs)
    return func_produre


class HelloWorld(Resource):
    """
    Demo resource
    """
    def get(self):
        return {'message': 'Hello World', 'code': 0}, 200


class User(Resource):
    """User Resource
    """
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('id', type=str, location='args',
                                   help='Id required')
        self.reqparse.add_argument('email', type=str, location='json',
                                   help='Email required')
        self.reqparse.add_argument('password', type=str, location='json',
                                   help='Password required, length between 8-16')
        self.reqparse.add_argument('first_name', type=str, location='json',
                                   help='First_name required')
        self.reqparse.add_argument('last_name', type=str, location='json',
                                   help='Last_name required')
        self.reqparse.add_argument('address', type=str, location='json',
                                   help='Address required')
        self.args = self.reqparse.parse_args()
        super(User, self).__init__()

    def get(self):
        try:
            id = self.args.get('id')
            if id is None:
                raise ValueError('ID is missing')
            user = models.User.get_user_by_id(id)
            if user is None:
                raise ValueError('User not exist')
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'user': user, 'code': 0}, 200

    def post(self):
        try:
            email = self.args.get('email')
            password = self.args.get('password')
            first_name = self.args.get('first_name')
            last_name = self.args.get('last_name')
            address = self.args.get('address')

            if models.User.check_exist_by_email(email):
                return {'message': 'Email existed', 'code': 1}, 400

            if not all([email, password, first_name, last_name, address]):
                raise ValueError('Bad request')
            user = models.User(first_name, last_name, email, password, address)
            user.save_to_db()
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'id': user.id, 'code': 0}, 200

    @auth
    def put(self, *args, **kwargs):
        try:
            user = models.User.query.filter_by(id=kwargs['id']).first()
            email = self.args.get('email')
            password = self.args.get('password')
            first_name = self.args.get('first_name')
            last_name = self.args.get('last_name')
            address = self.args.get('address')

            if email:
                user.email = email
            if password:
                user.password = password
            if first_name:
                user.first_name = first_name
            if last_name:
                user.last_name = last_name
            if address:
                user.address = address
            user.save_to_db()
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'message': 'Update success', 'code': 0}, 200

    @auth
    def delete(self, *args, **kwargs):
        try:
            user = models.User.query.filter_by(id=kwargs['id']).first()
            models.db.session.delete(user)
            models.db.session.commit()
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'message': 'Delete success', 'code': 0}, 200


class Group(Resource):
    """Group video call resource
    """
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('id', type= int,
                help='Id required', location='args')
        self.reqparse.add_argument('session', type=str,
                help='Session required', location='json')
        self.reqparse.add_argument('describe', type=str,
                help='Describe required', location='json')
        self.reqparse.add_argument('status', type=str,
                help='Status required', location='json')
        self.reqparse.add_argument('on_finish', type=str,
                help='On_finish required', )
        self.args = self.reqparse.parse_args()
        super(Group, self).__init__()

    def get(self):
        try:
           id = self.args.get('id')
           if id is None:
               raise ValueError('ID is missing')
           group = models.Group.get_group_by_id(id)
           if group == None:
               raise ValueError('Can not find group with id {}'.format(id))
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'group': group, 'code': 0}, 200

    @auth
    def post(self, *args, **kwargs):
        try:
            session = self.args.get('session')
            describe = self.args.get('describe')
            status = self.args.get('status')
            if not all([session, describe, status]):
                raise ValueError('Bad request')
            assert status in ['public', 'private'], 'Expected `private` or `public`'
            _gs = models.Group.query.filter_by(session=session).first()
            assert _gs is None, 'Session existed'
            group = models.Group(session, describe, status)
            group.save_to_db()
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'id': group.id, 'code': 0}, 200

    @auth
    def put(self, *args, **kwargs):
        try:
            user = models.User.query.filter_by(id=kwargs['id']).first()
            groupid = self.args.get('id')
            session = self.args.get('session')
            describe = self.args.get('describe')
            status = self.args.get('status')                 
            group = models.db.session.query(models.Group).with_parent(user).filter_by(id=groupid).first()
            if session:
                group.session = session
            if describe:
                group.describe = describe
            if status:
                group.status = status
            group.save_to_db()
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'message': 'Update success', 'code': 0}, 200
    @auth
    def delete(self, *args, **kwargs):
        try:
            group = models.Group.query.filter_by(id=kwargs['id']).first()
            models.db.session.delete(group)
            models.db.session.commit()
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'message': 'Delete success', 'code': 0}, 200

class Authentication(Resource):
    """Implement authentication module
    """
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email', type=str, required=True,
                                   help='Email required for login process')
        self.reqparse.add_argument('password', type=str, required=True,
                                   help='Password required for login process')
        self.args = self.reqparse.parse_args()
        super(Authentication, self).__init__()

    def post(self):
        """Implement login method
        """
        try:
            email = self.args.get('email')
            password = self.args.get('password')
            user = models.User.query.filter_by(email=email).first()
            if user is None:
                raise ValueError('User not exist')
            valid = user.verify_password(password)
            if not valid:
                raise ValueError('Password is wrong')
            payload = models.User.to_json(user)
            payload['expired'] = datetime.timestamp(datetime.now()) + JWT_TIME_EXPIRED
            token = jwt.encode(payload, JWT_KEY, JWT_ALGORITHM).decode('utf-8')
        except Exception as err:
            return {'message': str(err), 'code': 1}, 400
        else:
            return {'message': 'Login success', 'token': token, 'code': 0}, 200, {'Token': token}

    def delete(self):
        """Implement logout method
        """
        pass
