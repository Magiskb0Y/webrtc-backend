"""Implement socketio server
"""
import logging
import json
from flask import request, current_app, g
from flask_socketio import SocketIO, Namespace, emit, send, join_room, leave_room


socketio_log = logging.getLogger('socketio')
engineio_log = logging.getLogger('engineio')
gunicorn_logger = logging.getLogger('gunicorn.error')

list_room = []

io = SocketIO(async_mode='eventlet',
        logger=socketio_log,
        engineio_logger=engineio_log)


class ControllerNamespace(Namespace):
    """Namespace for controller socket
    """
    def on_connect(self):
        """Handle connect event
        """
        sid = request.sid
        # create single connect between server and user
        join_room(sid)
        gunicorn_logger.info('[User connected]: {}'.format(request.sid))

    def on_disconnect(self):
        """Handle disconnect event
        """
        gunicorn_logger.info('[User disconnected]: {}'.format(request.sid))

    def on_create_room(self, data):
        room = {
                'name': data['name'],
                'author': request.sid,
                'sdp': []
                }
        room['sdp'].append(data.get('sdp'))
        list_room.append(room)
        emit('room', {'contents': list_room})
        join_room(data['name'])
        gunicorn_logger.info("Room {} created and user {} join".format(room, request.sid))

    def on_icandidate(self, data):
        emit('icandidate', args=data, include_self=False)

    def on_join_room(self, data):
        c = False
        room = None
        for i in range(len(list_room)):
            c = list_room[i].get('name') == data.get('name')
            if c:
                room = list_room[i]
                break;
        if c:
            sid = request.sid
            join_room(data['name'])
            emit('join_room', room=data.get('name'), args={'sdp': room['sdp']}, include_self=False)
            gunicorn_logger.info("User {} join room {}".format(sid, data.get("name")))

    def on_leave_room(self, data):
        leave_room(data.get('room'))
        sid = request.sid
        emit('leave_room', room=data.get('room'), args={'sid': sid}, include_self=False)
        gunicorn_logger.info("User {} leave room {}".format(sid, data.get("room")))


io.on_namespace(ControllerNamespace('/socketio'))

