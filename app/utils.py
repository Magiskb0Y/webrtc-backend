"""
Contain help function for service
"""
from random import SystemRandom

__CHARS = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM!0123456789'


def generate_key(length):
    """
    Perform generate random key

    Parameters:
    -----------
    length: int, length of random key

    Returns:
    --------
    key: str
    """
    key = ''.join([SystemRandom().choice(__CHARS) for _ in range(length)])
    return key
