#!/bin/sh

set -e

supervisord -c supervisord.conf

exec "$@"
