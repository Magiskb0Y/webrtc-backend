"""
Run with development environment
"""
import os
import app

application = app.create_app(os.environ.get('WEBRC_BACKEND_ENV', 'DevelopmentConfig'))
application.run()
