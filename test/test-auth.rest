http://localhost:5000
GET /ping

http://localhost:5000
Content-Type: application/json
POST /auth
{
    "email": "test1@mail.com",
    "password": "123"
}

http://localhost:5000
GET /user

http://localhost:5000
Content-Type: application/json
POST /user
{
    "email": "test@mail.com",
    "password": "123",
    "first_name": "Test",
    "last_name": "app",
    "address": "VN"
}

http://localhost:5000
Content-Type: application/json
Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZW1haWwiOiJ0ZXN0QG1haWwuY29tIiwibmFtZSI6eyJmaXJzdF9uYW1lIjoiVGVzdCIsImxhc3RfbmFtZSI6ImFwcCJ9LCJhZGRyZXNzIjoiVk4iLCJvbl9yZWdpc3RlciI6IjEwLzE4LzE4LU9jdG9iZXItMjAxOCIsImV4cGlyZWQiOjE1Mzk5MTk4MzUuMDE4NjA1fQ.1kBFIIecF4aLdLmajIDh0d3QwUE82IMo1mEXvDttH-E
PUT /user
{
    "email": "test1@mail.com",
    "password": "123",
    "first_name": "Test",
    "last_name": "app",
    "address": "VN"
}

http://localhost:5000
Content-Type: application/json
Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZW1haWwiOiJ0ZXN0QG1haWwuY29tIiwibmFtZSI6eyJmaXJzdF9uYW1lIjoiVGVzdCIsImxhc3RfbmFtZSI6ImFwcCJ9LCJhZGRyZXNzIjoiVk4iLCJvbl9yZWdpc3RlciI6IjEwLzEyLzE4LU9jdG9iZXItMjAxOCIsImV4cGlyZWQiOjE1Mzk0MjE4NjYuMjcxMzIyfQ.rczWu9zTAtiNgPViQ2lcia61u2bgpYKl4xEVuigvm2M
DELETE /user
