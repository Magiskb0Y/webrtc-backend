"""Entry point for wsgi toolkit
"""
import logging

from app import create_app

server = create_app('ProductConfig')
gunicorn_logger = logging.getLogger('gunicorn.error')
server.logger.handlers = gunicorn_logger.handlers
server.logger.setLevel(gunicorn_logger.level)
